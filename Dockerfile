FROM jenkins/jenkins
USER root
#Define variables
ENV MAVEN_VERSION 3.9.4

#Update Base OS and install additional tools
RUN apt-get update && apt-get install -y wget \
    &&  wget --no-verbose https://downloads.apache.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz -P /tmp/ \
    && tar xzf /tmp/apache-maven-$MAVEN_VERSION-bin.tar.gz -C /opt/ \
    && ln -s  /opt/apache-maven-$MAVEN_VERSION /opt/maven \
    && ln -s /opt/maven/bin/mvn /usr/local/bin \
    && rm /tmp/apache-maven-$MAVEN_VERSION-bin.tar.gz  \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && chown jenkins:jenkins /opt/maven;
#Set up permissions
ENV MAVEN_HOME=/opt/mvn
USER jenkins

